import 'package:automator/config/backend/service.dart';
import 'package:automator/config/main.dart' show Config;
import 'package:flutter/material.dart' show WidgetsFlutterBinding, runApp;
import 'package:flutter_dotenv/flutter_dotenv.dart' show dotenv;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load();
  BackendService().init();
  runApp(Config());
}
