import 'package:flutter_dotenv/flutter_dotenv.dart' show dotenv;
import 'package:supabase_flutter/supabase_flutter.dart'
    show Supabase, SupabaseClient;

/// A service that provides access to Supabase.
class BackendService {
  /// Initializes Supabase
  Future<void> init() async => await Supabase.initialize(
        url: dotenv.env['SUPABASE_URL'],
        anonKey: dotenv.env['SUPABASE_ANON'],
      );

  /// The Supabase client
  SupabaseClient get client => Supabase.instance.client;
}
