import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:iconify_flutter/icons/mdi.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          'Please login to continue',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
        Container(
          padding: const EdgeInsets.all(5),
          margin: const EdgeInsets.only(bottom: 10),
          child: const TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              labelText: 'Email',
              labelStyle: TextStyle(
                color: Colors.black,
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          margin: const EdgeInsets.only(bottom: 25),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Password',
              labelStyle: TextStyle(
                color: Colors.black,
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ),
        GFButton(
          onPressed: () {},
          text: 'Login',
          shape: GFButtonShape.square,
          size: GFSize.LARGE,
          fullWidthButton: true,
          color: GFColors.SUCCESS,
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: const Text(
            'OR',
            textAlign: TextAlign.center,
          ),
        ),
        GFButton(
          onPressed: () {},
          text: 'Login in with Google',
          icon: const Iconify(
            Mdi.google,
            color: Colors.white,
            size: 20,
          ),
          shape: GFButtonShape.square,
          size: GFSize.LARGE,
          fullWidthButton: true,
          color: GFColors.DANGER,
          textColor: Colors.white,
        ),
        GFButton(
          onPressed: () {},
          text: 'Login in with Facebook',
          icon: const Icon(Icons.facebook, color: Colors.white),
          shape: GFButtonShape.square,
          size: GFSize.LARGE,
          fullWidthButton: true,
          color: GFColors.PRIMARY,
          textColor: Colors.white,
        ),
        Container(
          child: const Text('New here?'),
          margin: const EdgeInsets.symmetric(vertical: 10),
        ),
        GFButton(
          onPressed: () {},
          text: 'Sign up',
          shape: GFButtonShape.square,
          size: GFSize.LARGE,
          fullWidthButton: true,
          color: GFColors.SUCCESS,
          type: GFButtonType.outline,
        ),
      ],
    );
  }
}
